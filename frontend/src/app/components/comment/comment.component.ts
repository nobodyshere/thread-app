import { Component, Input, OnDestroy } from "@angular/core";
import { Comment } from "src/app/models/comment/comment";
import { User } from "src/app/models/user";
import { Subject, Observable, empty } from "rxjs";
import { AuthenticationService } from "src/app/services/auth.service";
import { AuthDialogService } from "src/app/services/auth-dialog.service";
import { LikeService } from "src/app/services/like.service";
import { CommentService } from "src/app/services/comment.service";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import { DialogType } from "src/app/models/common/auth-dialog-type";
import { Post } from "src/app/models/post/post";

@Component({
  selector: "app-comment",
  templateUrl: "./comment.component.html",
  styleUrls: ["./comment.component.sass"]
})
export class CommentComponent {
  @Input() public comment: Comment;
  @Input() public currentUser: User;
  @Input() public post: Post;

  private unsubscribe$ = new Subject<void>();

  public constructor(
    private authService: AuthenticationService,
    private authDialogService: AuthDialogService,
    private likeService: LikeService,
    private commentService: CommentService
  ) { }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public likeComment(isLiked: boolean) {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(
          switchMap(userResp =>
            this.likeService.likeComment(
              this.post,
              userResp,
              isLiked,
              this.comment
            )
          ),
          takeUntil(this.unsubscribe$)
        )
        .subscribe(comment => (this.comment = comment));
      return;
    }

    this.likeService
      .likeComment(this.post, this.currentUser, isLiked, this.comment)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(comment => (this.comment = comment));
  }

  private catchErrorWrapper(obs: Observable<User>) {
    return obs.pipe(
      catchError(() => {
        this.openAuthDialog();
        return empty();
      })
    );
  }

  public openAuthDialog() {
    this.authDialogService.openAuthDialog(DialogType.SignIn);
  }

  public getLikes(reaction: boolean) {
    return this.comment.reactions.filter(x => x.isLike == reaction).length;
  }

  public DeleteComment() {
    this.commentService.deleteComment(this.comment).subscribe();
  }
}
