import { PostService } from "./../../services/post.service";
import { MatSlideToggleChange, MatButtonToggleChange, MatButtonBase, MatButtonToggle } from "@angular/material";
import {
  Component,
  Input,
  OnDestroy,
  OnInit,
} from "@angular/core";
import { Post } from "src/app/models/post/post";
import { AuthenticationService } from "src/app/services/auth.service";
import { AuthDialogService } from "src/app/services/auth-dialog.service";
import { empty, Observable, Subject } from "rxjs";
import { DialogType } from "src/app/models/common/auth-dialog-type";
import { LikeService } from "src/app/services/like.service";
import { NewComment } from "src/app/models/comment/new-comment";
import { CommentService } from "src/app/services/comment.service";
import { User } from "src/app/models/user";
import { Comment } from "src/app/models/comment/comment";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.sass"]
})
export class PostComponent implements OnDestroy, OnInit {
  @Input() public post: Post;
  @Input() public currentUser: User;

  public showComments = false;
  public newComment = {} as NewComment;
  public isOnlyMine = false;
  public comments: Comment[] = [];
  public authorizedUser: User;
  public editComment = false;

  private unsubscribe$ = new Subject<void>();

  public constructor(
    private authService: AuthenticationService,
    private authDialogService: AuthDialogService,
    private likeService: LikeService,
    private commentService: CommentService,
    private snackBarService: SnackBarService,
    private postService: PostService
  ) { }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public ngOnInit() {
    this.getUser();
    console.log(this.currentUser);
    this.comments = this.post.comments;
  }

  public toggleComments() {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(user => {
          if (user) {
            this.currentUser = user;
            this.showComments = !this.showComments;
          }
        });
      return;
    }

    this.showComments = !this.showComments;
  }

  public likePost(isLiked: boolean) {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(
          switchMap(userResp =>
            this.likeService.likePost(this.post, userResp, isLiked)
          ),
          takeUntil(this.unsubscribe$)
        )
        .subscribe(post => (this.post = post));

      return;
    }

    this.likeService
      .likePost(this.post, this.currentUser, isLiked)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(post => (this.post = post));
  }

  public DeletePost() {
    this.postService.deletePost(this.post)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(x => this.ngOnInit());
  }

  public EditPost(event: MatButtonToggle) {

  }


  public sendComment() {
    this.newComment.authorId = this.currentUser.id;
    this.newComment.postId = this.post.id;

    this.commentService
      .createComment(this.newComment)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        resp => {
          if (resp) {
            this.post.comments = this.sortCommentArray(
              this.post.comments.concat(resp.body)
            );
            this.newComment.body = undefined;
          }
        },
        error => this.snackBarService.showErrorMessage(error)
      );
  }

  public openAuthDialog() {
    this.authDialogService.openAuthDialog(DialogType.SignIn);
  }

  private catchErrorWrapper(obs: Observable<User>) {
    return obs.pipe(
      catchError(() => {
        this.openAuthDialog();
        return empty();
      })
    );
  }

  private sortCommentArray(array: Comment[]): Comment[] {
    return array.sort(
      (a, b) => +new Date(b.createdAt) - +new Date(a.createdAt)
    );
  }

  public getLikes(reaction: boolean) {
    return this.post.reactions.filter(x => x.isLike == reaction).length;
  }


  private getUser() {
    this.authService
      .getUser()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => (this.currentUser = user));
  }
}
