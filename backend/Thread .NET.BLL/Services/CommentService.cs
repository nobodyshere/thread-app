﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public CommentService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(x => x.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(x => x.Id == commentEntity.Id);

            var comment = _mapper.Map<CommentDTO>(createdComment);
            await _postHub.Clients.All.SendAsync("CreateComment", comment, newComment.PostId);
            return comment;

        }

        public async Task DeleteComment(int id)
        {
            Comment comment = _context.Comments.FirstOrDefault(x => x.Id == id);
            List<CommentReaction> commentReactions = _context.CommentReactions.Where(x => x.Comment.Id == id).ToList();

            var commentEntity = _mapper.Map<Comment>(comment);
            var commentReactionEntity = _mapper.Map<List<CommentReaction>>(commentReactions);

            _context.CommentReactions.RemoveRange(commentReactionEntity);
            _context.Comments.RemoveRange(commentEntity);

            await _context.SaveChangesAsync();
        }
    }
}
