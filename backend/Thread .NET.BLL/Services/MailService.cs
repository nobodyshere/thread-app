﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.DAL.Context;
using AutoMapper;
using System.Threading.Tasks;
using System.Net;

namespace Thread_.NET.BLL.Services
{
    class MailService : BaseService
    {
        public MailService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public static async Task SendMail(string email, string subject, string body)
        {
            MailAddress from = new MailAddress("somemail@gmail.com", "BinaryStudio");
            MailAddress to = new MailAddress(email);
            MailMessage m = new MailMessage(from, to);
            m.Subject = subject;
            m.Body = body;
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.Credentials = new NetworkCredential("somemail@gmail.com", "password");
            smtp.EnableSsl = true;
            await smtp.SendMailAsync(m);
        }

    }
}
